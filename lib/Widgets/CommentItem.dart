import 'package:flutter/material.dart';
import '../Providers/Comments.dart';

class CommentItem extends StatelessWidget {
  final Comment comment;

  CommentItem(this.comment);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(8),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          border: Border.all(color: Colors.amber, width: 2),
          color: Colors.white),
      child: ListTile(
        leading: CircleAvatar(
          backgroundImage: NetworkImage(comment.imageUrl),
        ),
        title: Text(
          '${comment.content}',
          softWrap: true,
        ),
      ),
    );
  }
}
