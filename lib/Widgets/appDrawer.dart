import 'package:flutter/material.dart';
import 'package:uff/Routes/Loginscreen.dart';
import '../Providers/Auth.dart';
import 'package:provider/provider.dart';
class AppDrawer extends StatefulWidget {
  @override
  State createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(child: Column(children: <Widget>[
      SizedBox(height: 30,) ,
      ListTile(leading: Icon(Icons.exit_to_app), title: Text("log out"), onTap: () async{


       await Provider.of<Auth>(context , listen: false).logout().then((_){
         Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
       }) ;
      },)
    ],),);
  }
}
