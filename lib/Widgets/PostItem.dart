import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../Providers/Post.dart';
import '../Routes/postDetails.dart';

class PostItem extends StatelessWidget {
//  final PostModel postModel;
//
//  PostItem(this.postModel);

  @override
  Widget build(BuildContext context) {
    final post = Provider.of<PostModel>(context);
    return Container(
      margin: const EdgeInsets.all(2.3),
      child: GridTile(
          child: InkWell(
        onTap: () {
          Navigator.of(context)
              .pushNamed(PostDetails.routeName, arguments: post);
        },
        child: Image.network(
          post.imageUrl,
          fit: BoxFit.cover,
        ),
      )),
    );
  }
}
