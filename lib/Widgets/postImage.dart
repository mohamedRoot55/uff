import 'package:flutter/material.dart';

class PostImage extends StatelessWidget {
  final String imageUrl;

  final int likesCount;

  PostImage({this.likesCount, this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            height: 150,
            width: double.infinity,
            child: Image.network(imageUrl , fit: BoxFit.cover,),
          ),
          Positioned(
            child: Container(
              width: 50,
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.favorite_border,
                    color: Colors.amber,
                  ),
                  Text('$likesCount' , style: TextStyle(color: Colors.amber),)
                ],
              ),
            ),
            right: 10,
            bottom: 10,
          )
        ],
      ),
    );
  }
}
