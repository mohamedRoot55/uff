import 'package:flutter/cupertino.dart';

class Comment {
  String id;

  String userName;
  String imageUrl;

  String Content;

  DateTime dateTime;

  bool isLiked = false;

  Comment(
      {
      @required this.dateTime,
      @required this.imageUrl,
      @required this.Content,
      @required this.id,
      @required this.isLiked,
      @required this.userName});
      }
