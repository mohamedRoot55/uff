import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uff/Providers/Auth.dart';
import 'package:uff/Providers/Comments.dart';
import 'package:uff/Providers/Profiles.dart';
import 'package:uff/Routes/SplashScreen.dart';
import './Providers/Posts.dart';
import './Routes/HomeRoute.dart';
import './Routes/addNewPost.dart';
import './Routes/postDetails.dart';
import './Routes/Loginscreen.dart';
import './Routes/SignUpScreen.dart';
import './Routes/ConfigrationScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: Auth()),
        ChangeNotifierProxyProvider<Auth, Posts>(builder: (ctx, auth, posts) {
          return Posts(
              userId: auth.UserId,
              authToken: auth.token,
              items: posts != null ? posts.getItems : []);
        }),
        // ChangeNotifierProvider.value(value: Posts()),
        ChangeNotifierProxyProvider<Auth, Comments>(
            builder: (ctx, auth, comments) {
          return Comments(
              userId: auth.UserId,
              authToken: auth.token,
              allComment: comments != null ? comments.getItems : []);
        }),
        ChangeNotifierProxyProvider<Auth, Profiles>(
          builder: (ctx, auth, profiles) {
            return Profiles(
                authToken: auth.token,
                userId: auth.UserId,
                items: profiles != null ? profiles.getProfiles : []);
          },
        ) ,
        ChangeNotifierProvider.value(value: Profiles())  ,
      ],
      child: Consumer<Auth>(
        builder: (ctx, auth, child) {
          return MaterialApp(
            title: 'scial app',
            home: auth.isAuthenticate
                ? HomeRoute()
                : FutureBuilder(
                    future: auth.autoLogin(),
                    builder: (ctx, snapshot) =>
                        snapshot.connectionState == ConnectionState.waiting
                            ? SplashScreen()
                            : LoginScreen(),
                  ),
            routes: {
              AddNewPost.routeName: (ctx) => AddNewPost(),
              PostDetails.routeName: (ctx) => PostDetails(),
              LoginScreen.routeName: (ctx) => LoginScreen(),
              SignUpScreen.routeName: (ctx) => SignUpScreen(),
              HomeRoute.namedRoute: (ctx) => HomeRoute(),
              ConfigrationScreen.routeName: (ctx) => ConfigrationScreen()
            },
          );
        },
      ),
    );
  }
}
// todo : tomorrow i'll  fetch profile data in add new post screen and add it's data to postModel to catch it in future when entered to post details