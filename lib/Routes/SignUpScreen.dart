import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pigment/pigment.dart';
import 'package:provider/provider.dart';
import 'package:uff/Routes/HomeRoute.dart';
import '../Providers/Auth.dart';
import '../helpers/CustomException.dart';
import '../Routes/Loginscreen.dart';
import '../Routes/ConfigrationScreen.dart';
class SignUpScreen extends StatefulWidget {
  static const String routeName = "/signUp";

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final formKey = GlobalKey<FormState>();
  String username = '';

  String password = '';
bool isLoading = false ;
  final passwordController = TextEditingController();


  void _showErrorMessage(String message) {
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            title: Text('error has ocurred'),
            content: Text(message),
            actions: <Widget>[
              FlatButton(
                child: Text('okey'),
                onPressed: () {
                  Navigator.of(ctx).pop();
                },
              )
            ],
          );
        });
  }


  Future<void> saveForm() async {
    if (!formKey.currentState.validate()) {
      // Invalid!
      return;
    }
    formKey.currentState.save();
    setState(() {
      isLoading = true;
    });
    try {

        // Sign user up
        await Provider.of<Auth>(context)
            .signUp(username, password);
        Navigator.of(context).pushReplacementNamed(ConfigrationScreen.routeName);

    } on CustomException catch (error) {
      var errorMessage = 'Authentication failed';
      if (error.toString().contains('INVALID_EMAIL')) {
        errorMessage = 'this is not a vaild email adress';
      } else if (error.toString().contains('EMAIL_EXISTS')) {
        errorMessage = 'this email adress is already in use';
      } else if (error.toString().contains('WEAK_PASSWORD')) {
        errorMessage = 'this password is to weak';
      } else if (error.toString().contains('EMAIL_NOT_FOUND')) {
        errorMessage = 'Could not find a user with that email.';
      } else if (errorMessage.toString().contains('INVALID_PASSWORD')) {
        errorMessage = 'this password invalid';
      }

      _showErrorMessage(errorMessage);
    } catch (error) {
      const String errorMessage =
          'Could not authenticate you. Please try again later. ';
      _showErrorMessage(errorMessage);
    }

    setState(() {
      isLoading = false;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Pigment.fromString("#51EAFF"),
      body: isLoading ? Center(child: CircularProgressIndicator(),) :  Padding(
        padding: const EdgeInsets.all(25.0),
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                Container(
                  width: 120,
                  height: 120,
                  margin: const EdgeInsets.only(top: 20),
                  child: Image.asset("assets/images/flutter_logo.png"),
                ),
                TextFormField(
                  decoration: InputDecoration(
                    icon: Icon(Icons.person),
                    labelText: 'email',
                  ),
                  onSaved: (value) {
                    username = value;
                  },
                  validator: (value){
                    if(value.isEmpty){
                      return 'please enter email to confirm sign up' ;
                    }
                    if(!value.contains("@") || !value.contains(".com")){

                      return 'email should contains \@ & . ' ;
                    }
                    return null ;
                  }
                  ,

                ),
                TextFormField(
                  controller: passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    icon: Icon(Icons.lock_outline),
                    labelText: 'password',
                  ),
                  onSaved: (value) {
                    password = value;
                  },
                  validator: (value) {
                    if (value.isEmpty || value.length < 5) {
                      return 'Password is too short!';
                    }
                    return null;
                  },
                ),
                TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(
                      icon: Icon(Icons.lock_outline),
                      labelText: 'confirm password',
                    ),
                    validator: (value) {
                      if (value != passwordController.text) {
                        return 'Passwords do not match!';
                      }
                      return null;
                    }),
                SizedBox(
                  height: 40,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      // margin: const EdgeInsets.only(top: 20),
                      width: 160,
                      child: RaisedButton(
                        onPressed: saveForm,
                        child: Text(
                          "Create Account",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Colors.blue,
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15))),
                      ),
                    ),
                    SizedBox(
                      width: 11,
                    ),
                    Text(
                      "or",
                      style: TextStyle(color: Colors.grey),
                    ),
                    SizedBox(
                      height: 11,
                    ),
                    Container(
                      // height: 90,
                      child: RaisedButton(
                        onPressed: () {
                         Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
                        },
                        child: Text(
                          "LogIn",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Colors.blue,
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15))),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
