import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Providers/Post.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:provider/provider.dart';
import '../Providers/Posts.dart';
import 'package:path/path.dart' as path;
import 'dart:io';
import '../Providers/Profiles.dart';

class AddNewPost extends StatefulWidget {
  static const routeName = '/addNewPost';

  @override
  State createState() => _AddNewPostState();
}

class _AddNewPostState extends State<AddNewPost> {
  // todo declare some data
  File _image;
  String urlImage;
  bool isLoaded = false;
bool isInit = true ;
  String DefaultImageUrl;
  final formKey = GlobalKey<FormState>();

  //  @override
//  void dispose() {
//    super.dispose();
//    DefaultImageUrl = '';
//    _image = null;
//    formKey.currentState.dispose();
//  } // todo make post object and give it defaults value



  PostModel postModel = new PostModel(
      imageUrl: '',
      content: '',
      userImage: 'http://www.nretnil.com/avatar/LawrenceEzekielAmos.png');

  // todo show error message if user faild to add data to server
  void _showErrorMessage(String message, BuildContext context) {
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            title: Text('error has ocurred'),
            content: Text(message),
            actions: <Widget>[
              FlatButton(
                child: Text('okey'),
                onPressed: () {
                  Navigator.of(ctx).pop();
                },
              )
            ],
          );
        });
  }

  // todo save form and check validation first
  Future<void> _saveForm(BuildContext context) async {
    final postsProvider = Provider.of<Posts>(context);
    bool isValidate = formKey.currentState.validate();
    if (!isValidate) {
      return;
    }
    formKey.currentState.save();
    setState(() {
      isLoaded = true;
    });
    if (_image == null) {
      DefaultImageUrl = 'http://www.nretnil.com/avatar/LawrenceEzekielAmos.png';
      postModel.imageUrl = DefaultImageUrl;
      try {
        await postsProvider.addNewPost(postModel);
        setState(() {
          isLoaded = false;
        });
        Navigator.of(context).pop();

        // Navigator.of(context).pop();
      } catch (error) {
        throw error;
      }

      return;
    }
    try {
      UploadImage(context).then((value) {
        postsProvider.addNewPost(postModel);
        setState(() {
          isLoaded = false;
        });
        Navigator.of(context).pop();

      });
      // isLoaded =false ;
    } catch (error) {
      _showErrorMessage(
          "faild to add yout post to our database please check your internet connection ",
          context);
    }

//    setState(() {
//      formKey.currentState.dispose();
//    });
  }

//  @override
//  void didChangeDependencies() {
//    super.didChangeDependencies() ;
//
//  }

  @override
  void initState() {
    super.initState() ;
    Future.delayed(Duration(milliseconds: 0)).then((value){
      Provider.of<Profiles>(context).fetchProfilesData() ;
    });
  }

  // todo get image by access to camera or gallery
  Future<void> getImageFormCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  Future<void> getImageFormGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
  }

  Future<void> UploadImage(BuildContext context) async {
    String imagename = path.basename(_image.path);
    StorageReference reference =
        FirebaseStorage.instance.ref().child(imagename);
    StorageUploadTask uploadTask = reference.putFile(_image);
    String downloadUrl =
        await (await uploadTask.onComplete).ref.getDownloadURL();
    // var downloadUrl = await reference.getDownloadURL();

      urlImage = downloadUrl.toString();
      postModel.imageUrl = urlImage;
    print(urlImage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(" add new post"),
      ),
      body: isLoaded
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.all(10.0),
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Container(
                          height: 150,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(15.0),
                              ),
                              border: Border.all(color: Colors.blue)),
                          child: _image == null
                              ? Center(
                                  child: Image.asset(
                                    "assets/images/avatar.png",
                                    fit: BoxFit.cover,
                                  ),
                                )
                              : Image.file(
                                  _image,
                                  fit: BoxFit.cover,
                                ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Form(
                          key: formKey,
                          child: Container(
                            padding: EdgeInsets.all(8.0),
                            height: 100,
                            width: double.infinity,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(15.0),
                                ),
                                border: Border.all(color: Colors.blue)),
                            child: Center(
                                child: TextFormField(
                              decoration: InputDecoration(
                                  labelText: 'what is in your mind'),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'please enter the post content';
                                }
                                return null;
                              },
                              onSaved: (value) {
                                postModel = new PostModel(
                                  content: value,
                                  imageUrl: postModel.imageUrl,
                                  userImage: postModel.userImage,
                                );
                              },
                            )),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              onPressed: () async {
                                await getImageFormGallery();
                              },
                              color: Colors.purple,
                              child: Text(
                                "select photo",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            RaisedButton.icon(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                onPressed: () async {
                                  await getImageFormCamera();
                                },
                                icon: Icon(
                                  Icons.camera,
                                  color: Colors.blue,
                                ),
                                label: Text("take photo"))
                          ],
                        )
                      ],
                    ),
                    Container(
                        margin: EdgeInsets.all(0),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(15.0)),
                          child: Text("post"),
                          color: Colors.amber,
                          // todo final save to data into server
                          onPressed: () => _saveForm(context),
                          elevation: 0,
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                        )),
                  ],
                ),
              ),
            ),
    );
  }
}
