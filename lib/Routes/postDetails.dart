import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uff/Providers/Post.dart';
import '../Widgets/postImage.dart';
import '../Providers/Comments.dart';
import '../Widgets/CommentItem.dart';

class PostDetails extends StatefulWidget {
  static const routeName = '/PostDetails';

  @override
  _PostDetailsState createState() => _PostDetailsState();
}

class _PostDetailsState extends State<PostDetails> {
  final controller = TextEditingController();
 bool isLoaded = false;
//  bool firstload = true;

  PostModel post;

  String text;


  @override
  void didChangeDependencies() async{
    super.didChangeDependencies();
    post = ModalRoute.of(context).settings.arguments;


    try {
      // isLoaded = true ;
     await  Provider.of<Comments>(context).fetchData(post.id) ;

    } catch (error) {
      throw error;
    }
//
//    firstload = false ;
//    isLoaded = false ;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("post details"),
      ),
      body: Container(
        margin: const EdgeInsets.all(3.4),
        child: Column(
          children: <Widget>[
            PostImage(
              likesCount: post.likesCount,
              imageUrl: post.imageUrl,
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.only(top: 20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 80,
                        width: 80,
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(post.userImage),
                        ),
                      )
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                       Container(
                              margin: const EdgeInsets.only(right: 20),
                              height: 220,
                              width: 230,
//                        color: Colors.amber,
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(
                                    15,
                                  )),
                                  color: Colors.amber,
                                  border: Border.all(
                                      color: Colors.amber, width: 2)),
                              child: isLoaded ? Center(child: Text("no comments added for this post"),):    Consumer<Comments>(
                                      builder: (_, comments, child) {
                                        List<Comment> allItems =
                                            comments.getItems;
                                        return ListView.builder(
                                          itemBuilder: (_, index) {
                                            return CommentItem(allItems[index]);
                                          },
                                          itemCount: allItems.length,
                                        );
                                      },
                                    ),
                            )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(left: 10),
                    child: TextField(
                      decoration: InputDecoration(
                        labelText: 'write comment',
                      ),
                      controller: controller,
                    ),
                  ),
                ),
                Consumer<Comments>(
                  builder: (ctx, comments, child) {
                    return IconButton(
                      icon: Icon(Icons.send),
                      onPressed: () async {
                        setState(() {
                          text = controller.text;
                        });
                        comments
                            .addComment(
                                postId: post.id,
                                content: text,
                                commentCreator: '',
                                imageUrl: post.userImage)
                            .then((value) {
                          controller.text = '';
                        });
                      },
                    );
                  },
                )
              ],
            )
          ],
          crossAxisAlignment: CrossAxisAlignment.start,
        ),
      ),
    );
  }
}
