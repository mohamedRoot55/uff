import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uff/Routes/HomeRoute.dart';
import '../Providers/Profiles.dart';
import '../Models/Profile.dart';
import 'package:image_picker/image_picker.dart';

import 'package:path/path.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';

class ConfigrationScreen extends StatefulWidget {
  static const String routeName = '/ConfigrationScreen';

  @override
  State createState() => _ConfigrationScreenState();
}

class _ConfigrationScreenState extends State<ConfigrationScreen> {
  bool isLoaded = false;
  File _image;

  String urlImage;

  final formKey = GlobalKey<FormState>();
  Profile profile = new Profile(imageUrl: '', name: '', phoneNumber: '');

//
//  @override
//  void dispose() {
//    formKey.currentState.dispose() ;
//    super.dispose() ;
//
//  }

  Future<void> _saveForm(BuildContext context) async {
    isLoaded = true;
    // formKey.currentState.validate();
    if (!formKey.currentState.validate()) {
      return;
    }
    formKey.currentState.save();
    if (_image == null) {
      profile.imageUrl =
          "http://www.nretnil.com/avatar/LawrenceEzekielAmos.png";
    } else {
      await uploadImage(context);
    }

    try {
      Provider.of<Profiles>(context).addNewProfileData(profile).then((_) {
        Navigator.of(context).pushReplacementNamed(HomeRoute.namedRoute);
      });
    } catch (error) {
      throw error;
    }
  }

  Future<void> getImageFormGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
  }

  Future<void> uploadImage(BuildContext context) async {
    String imagename = basename(_image.path);
    StorageReference reference =
        FirebaseStorage.instance.ref().child(imagename);
    StorageUploadTask uploadTask = reference.putFile(_image);
    String downloadUrl =
        await (await uploadTask.onComplete).ref.getDownloadURL();
    // var downloadUrl = await reference.getDownloadURL();

    urlImage = downloadUrl.toString();
    profile.imageUrl = urlImage;

    // print(urlImage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: isLoaded
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Form(
                key: formKey,
                child: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: double.infinity,
                    //

                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.all(7),
                          child: Column(
                            children: <Widget>[
                              Container(
                                height: 120,
                                width: 120,
                                child: Stack(
                                  fit: StackFit.expand,
                                  children: <Widget>[
                                    CircleAvatar(
                                      backgroundImage: _image == null
                                          ? AssetImage(
                                              "assets/images/avatar.png")
                                          : FileImage(_image),
                                    ),
                                    Positioned(
                                      right: 4,
                                      bottom: 4,
                                      child: Container(
                                          padding: EdgeInsets.all(0),
                                          // color: Theme.of(context).accentColor,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(30),
                                            color: Colors.amber,
                                          ),
                                          constraints: BoxConstraints(
                                            minWidth: 18,
                                            minHeight: 18,
                                          ),
                                          child: IconButton(
                                            padding: EdgeInsets.all(0),
                                            onPressed: () {
                                              getImageFormGallery();
                                            },
                                            icon: Icon(Icons.add),
                                          )),
                                    )
                                  ],
                                ),
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                  icon: Icon(
                                    Icons.person,
                                    color: Colors.purple,
                                  ),
                                  labelText: 'user name',
                                ),
                                onSaved: (value) {
                                  profile = Profile(
                                      name: value, imageUrl: profile.imageUrl);
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'please enter your name to confirm sign up';
                                  }
                                  return null;
                                },
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                  icon: Icon(
                                    Icons.phone,
                                    color: Colors.purple,
                                  ),
                                  labelText: 'phone number',
                                ),
                                onSaved: (value) {
                                  profile = Profile(
                                      name: profile.name, phoneNumber: value);
                                },
                                validator: (value) {
                                  if (int.tryParse(value) == null) {
                                    return 'this is a wrong phone number';
                                  }
                                  return null;
                                },
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          // padding: const EdgeInsets.all(0),
                          margin: const EdgeInsets.only(bottom: 0),
                          child: RaisedButton.icon(
                              color: Colors.amber,
                              onPressed: () async {
                                await _saveForm(context);
                              },
                              icon: Icon(Icons.done),
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap,
                              label: Text("save")),
                        )
                      ],
                    ),
                  ),
                ),
              ));
  }
}
