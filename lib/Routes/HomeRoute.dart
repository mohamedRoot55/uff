import 'package:flutter/material.dart';
import 'package:uff/Providers/Post.dart';
import 'package:uff/Widgets/appDrawer.dart';
import '../Routes/addNewPost.dart';
import 'package:provider/provider.dart';
import '../Providers/Posts.dart';
import '../Widgets/PostItem.dart';

class HomeRoute extends StatefulWidget {
  static const String namedRoute = '/Home';

  @override
  State createState() => _HomeRouteState();
}

class _HomeRouteState extends State<HomeRoute> {
  bool isInit = true;
  bool isLoaded = false ;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didChangeDependencies() async {
    // TODO: implement didChangeDependencies

    if (isInit) {

      try {
        await Provider.of<Posts>(context).fetchDataFromServer();
      } catch (error) {
       // isLoaded = true ;
      //  throw error;
      }
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(drawer: AppDrawer(),
      appBar: AppBar(
        title: Text("home"),
      ),
      body: Container(
        child: Consumer<Posts>(
          builder: (_, posts, child) {
            List<PostModel> allPosts = posts.getItems;
            if (isLoaded) {
              return Center(child: Text("no posts found"));
            } else
              return allPosts.length == 0
                  ? Center(child: Text("no posts found"))
                  : GridView.builder(
                      itemCount: allPosts.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 3 / 2,
                          crossAxisSpacing: 20,
                          mainAxisSpacing: 10),
                      itemBuilder: (ctx, index) {
                        return ChangeNotifierProvider.value(
                          value: allPosts[index],
                          child: PostItem(),
                        );
                      });
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushNamed(AddNewPost.routeName);
        },
        child: Icon(Icons.add),
      ),
    );
  }
//
//  Future<void> FetchData() async{
//  await  Provider.of<Posts>(context).fetchDataFromServer();
//  }
}
