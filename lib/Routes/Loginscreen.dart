import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pigment/pigment.dart';
import 'package:uff/Routes/HomeRoute.dart';
import 'package:uff/Routes/SignUpScreen.dart';
import 'package:provider/provider.dart';
import '../Providers/Auth.dart';

class LoginScreen extends StatefulWidget {
  static const String routeName = "/login";
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final formkey = GlobalKey<FormState>();
  String email ;
  String password ;

  @override
  void dispose() {
    super.dispose();
   // formkey.currentState.dispose();
  }
  void saveForm(){
    formkey.currentState.save() ;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Pigment.fromString("#51EAFF"),
      body: Padding(
        padding: const EdgeInsets.all(25.0),
        child: SingleChildScrollView(
          child: Form(
            key: formkey,
            child: Column(
              children: <Widget>[
                Container(
                  width: 120,
                  height: 120,
                  margin: const EdgeInsets.only(top: 20),
                  child: Image.asset("assets/images/flutter_logo.png"),
                ),
                TextFormField(
                  decoration: InputDecoration(
                    icon: Icon(Icons.person),
                    labelText: 'user name',
                  ),onSaved: (value){
                    email = value ;
                },
                ),
                TextFormField(obscureText: true,
                  decoration: InputDecoration(
                    icon: Icon(Icons.lock_outline),
                    labelText: 'password',
                  ),
                  onSaved: (value){
                    password = value ;
                  },
                ),
                Container(
                  margin: const EdgeInsets.only(top: 20),
                  width: 120,
                  child: RaisedButton(
                    onPressed: () {
                      //.......
                      saveForm();
                      Provider.of<Auth>(context).login(email, password).then((_){

                        Navigator.of(context).pushReplacementNamed(HomeRoute.namedRoute) ;
                      }) ;
                    },
                    child: Text(
                      "login",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Colors.blue,
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "or",
                  style: TextStyle(color: Colors.grey),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(

                  width: 120,
                  child: RaisedButton(
                    onPressed: () {
                     Navigator.of(context).pushNamed(SignUpScreen.routeName) ;
                    },
                    child: Text(
                      "sign Up",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Colors.blue,
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
