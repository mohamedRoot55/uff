import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './Post.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../helpers/CustomException.dart';

class Posts with ChangeNotifier {
  String authToken ;
  var userId ;
  List<PostModel> items = [];
  Posts({this.authToken , this.userId , this.items}) ;


  List<PostModel> get getItems {
    return [...items];
  }

  Future<void> addNewPost(PostModel postModel) async {
    final String uri = "https://fucking-app-f619e.firebaseio.com/Posts.json?auth=$authToken";
    try {
      final response = await http.post(
        uri,
        body: json.encode(
          {
            'userName' : postModel.userName ,
            'userImage': postModel.userImage,
            'imageUrl': postModel.imageUrl,
            'likesCount': 0,
            'datetime': DateTime.now().toIso8601String(),
            'content': postModel.content,

          },
        ),
      );

      notifyListeners();
      if (response.statusCode >= 400) {
        throw CustomException("faild to add post");
      }
    } catch (error) {
      throw error;
    }
  }

  Future<void> fetchDataFromServer() async {
    final String uri = "https://fucking-app-f619e.firebaseio.com/Posts.json?auth=$authToken";
    try {
      final Response = await http.get(uri);
      final LoadedData = json.decode(Response.body) as Map<String, dynamic>;
      if(Response == null ){

        items = [] ;
      }
      List<PostModel> FetchedData = [];
      LoadedData.forEach((PostId, PostData) {
        FetchedData.add(PostModel(
          userName: PostData['userName'],
          likesCount: PostData['likesCount'],
          content: PostData['content'],
          imageUrl: PostData['imageUrl'],
          userImage: PostData['userImage'],
          id: PostId,
          dateTime: DateTime.parse(PostData['datetime']),
        ));
      });
      // print(json.decode(Response.body)) ;
      items = FetchedData;
      notifyListeners();
    } catch (error) {
      CustomException(error) ;
    }
  }
}
