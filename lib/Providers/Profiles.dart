import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../Models/Profile.dart';

class Profiles with ChangeNotifier {
  String userId ;
  String authToken;
  List<Profile> items = [];

  Profiles({this.authToken , this.items , this.userId});



  List<Profile> get getProfiles {
    return [...items];
  }

  Future<void> addNewProfileData(Profile profile) async {
    final String url =
        "https://fucking-app-f619e.firebaseio.com/Profiles/$userId.json?auth=$authToken";
    try {
      http.post(url,
          body: json.encode({
            'name': profile.name,
            'phoneNumber': profile.phoneNumber,
            'imageUrl': profile.imageUrl,
          }));
    } catch (error) {
      throw error;
    }
  }
  Future<void> fetchProfilesData() async {
    final String url =
        "https://fucking-app-f619e.firebaseio.com/Profiles.json?auth=$authToken";
    final Response = await http.get(url) ;
    print(json.decode(Response.body)) ;
  }
  Future<void> fetchProfileData() async {
    final String url =
        "https://fucking-app-f619e.firebaseio.com/Profiles/$userId.json?auth=$authToken";
    final Response = await http.get(url) ;
    print(json.decode(Response.body)) ;
  }
}
