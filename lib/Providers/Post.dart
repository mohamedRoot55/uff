import 'package:flutter/material.dart';

class PostModel with ChangeNotifier {
  String id;

  String userName;

  String imageUrl;

  String userImage;

  int likesCount;

  final String content;

  DateTime dateTime;

  bool isLiked = false;

  PostModel(
      {this.dateTime,
      this.id,
      this.userName,
      this.likesCount,
      this.userImage,
      @required this.content,
      @required this.imageUrl,
      this.isLiked});
}
