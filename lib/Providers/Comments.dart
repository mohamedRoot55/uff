import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../helpers/CustomException.dart';
class Comment with ChangeNotifier {
  String content = '';
  DateTime dateTime;
  String commentId;
  String imageUrl;

  String commentCreator;

  Comment(
      {this.dateTime,
      this.content,
      this.commentCreator,
      this.commentId,
      this.imageUrl});
}

class Comments with ChangeNotifier {
  String authToken ;
  var userId ;
  List<Comment> allComment = []  ;
  Comments({this.userId , this.authToken , this.allComment});


  String postId;



  List<Comment> get getItems {
    return [...allComment];
  }

  Future<void> addComment(
      {String postId,
      String content,
      String commentCreator,
      String imageUrl}) async {
    final String url =
        'https://fucking-app-f619e.firebaseio.com/comments/$postId.json?auth=$authToken';
    try {
      http.post(url,
          body: json.encode({
            'content': content,
            'datetime': DateTime.now().toIso8601String(),
            'commentCreator': commentCreator,
            'imageUrl': imageUrl,
          }));
      this.postId = postId;
    } catch (error) {
      throw error;
    }
  }

  Future<void> fetchData(String postId) async {
    final String url =
        'https://fucking-app-f619e.firebaseio.com/comments/$postId.json?auth=$authToken';

    try {
      final Response = await http.get(url);
      final LoadedData = json.decode(Response.body) as Map<String, dynamic>;
      if(LoadedData == null) {
        allComment = [] ;
        return ;
      }
      List<Comment> ExtractedData = [];
      LoadedData.forEach((postId, postData) {
        ExtractedData.add(Comment(
            commentId: postId,
            commentCreator: postData['commentCreator'],
            content: postData['content'],
            dateTime: DateTime.parse(postData['datetime']),
            imageUrl: postData['imageUrl']));
      });
      allComment = ExtractedData;
      notifyListeners();
    } catch (error) {
      throw CustomException(error.toString());
    }
  }
}
