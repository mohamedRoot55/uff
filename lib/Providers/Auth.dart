import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import '../helpers/CustomException.dart';
class Auth with ChangeNotifier {
  String _token;

  DateTime _expireDate;

  String _userId;

  bool get isAuthenticate {
    return token != null;
  }

  String get token {
    if (_token != null &&
        _expireDate.isAfter(DateTime.now()) &&
        _expireDate != null) {
      return _token;
    }
    return null;
  }

  String get UserId {
    return _userId;
  }

  Future<void> _authenticate(
      String email, String password, String urlSegment) async {
    final String url =
        "https://identitytoolkit.googleapis.com/v1/accounts:$urlSegment?key=AIzaSyAVqxyLf2jPXNfeuAlyVPP4OU4GWchAcCA";
    try {
      final response = await http.post(url,
          body: json.encode({
            'email': email,
            'password': password,
            'returnSecureToken': true
          }));
      final responseData = json.decode(response.body);
      print(responseData) ;
      if (responseData['error'] != null) {
        throw CustomException(responseData['error']['message']);
      }
      _token = responseData['idToken'];
      _userId = responseData['localId'];
      _expireDate = DateTime.now()
          .add(Duration(seconds: int.parse(responseData['expiresIn'])));
      //  print(json.decode(response.body));

      final prefData = await SharedPreferences.getInstance();
      String value = json.encode({
        'AuthToken': _token,
        'UserId': _userId,
        'ExpierdData': _expireDate.toIso8601String(),
      });

      prefData.setString("UserData", value);
      notifyListeners();

    } catch (error) {
      throw error;
    }
  }

  Future<void> signUp(String email, String password) async {
    return _authenticate(email, password, 'signUp');
  }

  Future<void> login(String email, String password) async {
    return _authenticate(email, password, 'signInWithPassword');
  }

  Future<void> logout() async{
    _token = null;
    _userId = null;
    _expireDate = null;
    final prefData = await SharedPreferences.getInstance() ;
    prefData.clear() ;

    notifyListeners();
  }

  Future<void> autoLogin() async {
    final prefData = await SharedPreferences.getInstance();
    if (!prefData.containsKey("UserData")) {
      return false;
    }
    final user_data =
    json.decode(prefData.getString("UserData")) as Map<String, Object>;
    final expieryDate = DateTime.parse(user_data['ExpierdData']) ;
    if(expieryDate.isBefore(DateTime.now())){
      return false ;
    }
    _token = user_data['AuthToken'] ;
    _userId = user_data['UserId'] ;
    _expireDate = expieryDate ;
    notifyListeners() ;
    return true ;


  }
}
